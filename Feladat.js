net = require('net');
const uuidv1 = require('uuid/v1');

var clients = {}; // Con

// Start a TCP Server
net.createServer(function (sock) {

    clients[sock.fd] = sock; // Add the client, keyed by fd.
    sock.on('close', function() {
        delete clients[sock.fd]; // Remove the client.
    });
}).listen(5000);

setInterval(function() {
    for (i in clients) {
        sock = clients[i];
        if (sock.writable) {
            sock.write(new Date().toString() + "\n");
            sock.write(uuidv1() + "\n");
        }
    }
}, 1000);

