'use strict';

//Model

const uuid = require('node-uuid');


module.exports = class User {
    constructor(name) {
        this._id = uuid.v4();
        this._createdAt = new Date();
        this._updatedAt = new Date();

        if (name && name.trim().length > 0) {
            this._name = name;
        } else {
            throw new Error('Empty name!');
        }
    }

    getId() {
        return this._id;
    }

    setName(name) {
        if (name.trim().length > 0) {
            this._name = name;
            this._updatedAt = new Date();
        } else {
            throw new Error('Empty name!');
        }
    }

    toJSON() {
        return {
            id: this._id,
            name: this._name,
            createdAt: this._createdAt,
            updatedAt: this._updatedAt,
        };
    }
};
