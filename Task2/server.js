'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const uuid = require('node-uuid');
const User =require('./User');
var exphbs  = require('express-handlebars');


const app = express();
const port = process.env.PORT || 3000;


app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');


let users = [];



//Controll

// Settings
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));

console.log('Bind middleware');
app.use(function (req, res, next) {
    console.log(`Request - ${req.method}: ${req.originalUrl}`);

    // What is inside the body
    if (req.method === 'POST') {
        console.log(`Request - body: ${JSON.stringify(req.body)}`);
    }
    return next();
});

console.log('Bind routes');
app.get('/', function (req, res) {
    res.render('index');
});
/**
 * user megjelenitése
 */
app.get('/users', function (req, res) { //
    let response = [];
    console.log(users);
    if(users.length>0){
        users.forEach(function (user) {
            console.log(user);
            response.push(user.toJSON());
        });
        res.render("users",{layout:"main", users:response});

    }else{
        res.writeHead(301,
            {Location: 'http://localhost:3000'}
        );
        res.end();

    }


});
/**
 * adott user
 */
app.get('/users/:userId', function (req, res) {
    const params = req.body;
    let userk;
    let response = [];
    console.log(req.params); //kerdes??
    users.forEach(function (user) {
        console.log(user.getId());
        if (user.getId() === req.params.userId) {
            userk=user;
        }
    });
    if(userk){

        //return res.json(userk.toJSON());
        response.push(userk.toJSON());
        return res.render("userF",{layout:"main", users:response});
    }
    res.json({
        error: 'User not found'
    });


});

/**
 * fekűlvétel
 */
app.post('/users', function (req, res) { //
    const params = req.body;
    console.log("uszenet megerkezet");
    let user = new User(params.name);
    users.push(user);
   // res.json(user.toJSON());


    res.writeHead(301,
        {Location: 'http://localhost:3000'}
    );
    res.end();


});
/**
 * updatelni
 */
app.put('/users/:userId', function (req, res) {
    const params = req.body;
   // console.log(params);
    let user;

    users.forEach(function (u) {
        if (u.getId() === req.params.userId) {
            user = u;
        }
    });
    if (!user) {
        res.json({
            error: 'User not found'
        });
    }

    user.setName(params.name);
    res.writeHead(301,
        {Location: 'http://localhost:3000'}
    );
    res.end();
});
/**
 * törlés
 */
app.delete('/users/:userId', function (req, res) {
    const params = req.body;
   // console.log(params);
  //  console.log("szia delete");
    var newusers = [];

    users.forEach(function (u) {
     //   console.log(u);
        if (u.getId() === req.params.userId) {
        }else {
            newusers.push(u);
        }
    });
    users=[];
    console.log(newusers);
    users=newusers;
    //console.log("test");
    //console.log(users);
    res.writeHead(301,
        {Location: 'http://localhost:3000'}
    );
    res.end();
});




// Start the server
app.listen(port);
console.log('Express app started on port ' + port);
